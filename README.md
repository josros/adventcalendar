# Advent Calendar

Digital Advent Calendar Web Component.

[Demo](https://josros.gitlab.io/adventcalendar/)


## Fill-in your content

**The only thing you need to do is to implement [doorTemplates](./src/doorTemplates.ts).**

The Web Components provided under [./src/components/josrosAdventCalendar/contents/](./src/components/josrosAdventCalendar/contents/) may help you with that. There are components for e.g. audio, video and image content. Here is an example how you could implement a single door template:

```ts
function doorOne(): TemplateResult {
  return html`<div>
    <josros-image src="${importedImage}" alt="Your image description" subtitle="Your image subtitle"></josros-image>
  </div>`;
}
```

If you want to enable the *"door does not open before the actual date feature"* just set the `RESTRICT_BY_DATE` attribute in [josrosAdventDoor.ts](./src/components/josrosAdventCalendar/josrosAdventDoor.ts) to `true`.

In case you want to change the arrangement of the door numbers in the calendar, have a look at the `numbers` array in: [josrosAdventCalendar.ts](./src/components/josrosAdventCalendar/josrosAdventCalendar.ts)

## Execution

```bash
# install dependencies
npm install

# format and lint
npm run lint

# start develop mode (local execution)
npm start

# build for production
npm run build
```

## Credits

<span>Background photo by <a href="https://unsplash.com/@kierancwhite?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Kieran White</a> on <a href="https://unsplash.com/s/photos/christmas?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>