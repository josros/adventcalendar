export class ResizeObservation {
  private readonly onResizeLambda!: () => void;
  private resizeObserver?: ResizeObserver;

  constructor(resizeLambda: () => void) {
    this.onResizeLambda = resizeLambda;
  }

  public attach(container: HTMLElement): void {
    if (ResizeObserver) {
      this.resizeObserver = new ResizeObserver(this.onResizeLambda);
      this.resizeObserver.observe(container);
    } else {
      window.addEventListener('resize', this.onResizeLambda);
    }
  }

  public detach(): void {
    if (ResizeObserver) {
      this.resizeObserver?.disconnect();
    } else {
      window.removeEventListener('resize', this.onResizeLambda);
    }
  }
}
