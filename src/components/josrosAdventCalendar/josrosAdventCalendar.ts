import { LitElement, html, property, customElement, TemplateResult, CSSResult, css } from 'lit-element';
import './josrosAdventLayout.ts';
import './josrosOverlay.ts';
import { template, templateStyles } from '../../doorTemplates.ts';

@customElement('josros-advent-calendar')
export class AdventCalendar extends LitElement {
  // [
  //   1,
  //   2,
  //   3,
  //   4,
  //   5,
  //   6,
  //   7,
  //   8,
  //   9,
  //   10,
  //   11,
  //   12,
  //   13,
  //   14,
  //   15,
  //   16,
  //   17,
  //   18,
  //   19,
  //   20,
  //   21,
  //   22,
  //   23,
  //   24
  // ]
  private static readonly numbers = [
    7,
    23,
    1,
    20,
    22,
    5,
    2,
    24,
    12,
    14,
    8,
    17,
    21,
    4,
    19,
    10,
    15,
    13,
    9,
    6,
    18,
    11,
    3,
    16
  ];

  @property({ type: Boolean })
  private showOverlay = false;

  private currentDoor = 1;

  private doorOpened(ev: CustomEvent): void {
    // console.log(`${ev.detail} door opened`);
    this.currentDoor = ev.detail;
    this.showOverlay = true;
  }

  private overlayClosed(): void {
    this.showOverlay = false;
  }

  render(): TemplateResult {
    return html`
      <div class="calendar-container ${this.showOverlay ? 'noscroll' : ''}">
        <josros-advent-layout .numbers=${AdventCalendar.numbers} @doorOpened=${this.doorOpened}></josros-advent-layout>
        <josros-overlay .open=${this.showOverlay} @josros-overlay-closed="${this.overlayClosed}">
          ${this.renderDoor()}
        </josros-overlay>
      </div>
    `;
  }

  renderDoor(): TemplateResult {
    return template(this.currentDoor);
  }

  static get styles(): CSSResult[] {
    return [
      css`
        :host {
          font-size: var(--josros-advent-calendar--font-size, 1rem);
        }
        .noscroll {
          overflow: hidden;
        }
      `,
      templateStyles()
    ];
  }
}
