import { LitElement, html, customElement, TemplateResult, CSSResult, css, property, query } from 'lit-element';
import '@material/mwc-icon-button';

@customElement('josros-audio')
export class JosrosAudio extends LitElement {
  @property({ type: String })
  private src!: string;

  @query('audio')
  private audio!: HTMLAudioElement;

  firstUpdated(): void {
    window.addEventListener('josros-overlay-closed', () => this.audio.pause());
  }

  render(): TemplateResult {
    return html` <div class="audio-container">
      <audio controls>
        <source src="${this.src}" type="audio/mp3" />
      </audio>
    </div>`;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-audio--font-size, 1rem);
      }
      .audio-container {
        padding: 1em;
      }
    `;
  }
}
