import { LitElement, html, customElement, TemplateResult, CSSResult, css, property } from 'lit-element';
import './josrosContainer.ts';

@customElement('josros-image')
export class Image extends LitElement {
  @property({ type: String })
  private src!: string;

  @property({ type: String })
  private alt!: string;

  @property({ type: String })
  private subtitle?: string;

  render(): TemplateResult {
    return html`<josros-container>
      <div class="image-container">
        <slot name="top"></slot>
        <img class="image" src="${this.src}" alt="${this.alt}" />
        ${this.subtitle ? html`<div class="subtitle-container">${this.subtitle}</div>` : ''}
        <slot name="bottom"></slot>
      </div>
    </josros-container>`;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-image--font-size, 1rem);
      }
      .image-container {
        margin: auto;
        max-width: 80em;
      }
      .image {
        width: 100%;
      }
      .subtitle-container {
        padding-top: 1.5em;
        font-size: 1.6em;
        font-weight: 300;
      }
    `;
  }
}
