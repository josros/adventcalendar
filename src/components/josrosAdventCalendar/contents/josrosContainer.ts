import { LitElement, html, customElement, TemplateResult, CSSResult, css } from 'lit-element';

@customElement('josros-container')
export class Container extends LitElement {
  render(): TemplateResult {
    return html`<div class="container">
      <slot></slot>
    </div> `;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-container--font-size, 1rem);
      }
      .container {
        padding: 1.5em 1em;
      }
    `;
  }
}
