import { LitElement, html, customElement, TemplateResult, CSSResult, css, property } from 'lit-element';
import './josrosContainer.ts';

@customElement('josros-footer')
export class Footer extends LitElement {
  @property({ type: String })
  private text!: string;

  render(): TemplateResult {
    return html`<josros-container>
      <span class="footer-text">${this.text}</span>
    </josros-container> `;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-footer--font-size, 1rem);
      }
      .footer-text {
        font-size: 1.6em;
        font-weight: 300;
      }
    `;
  }
}
