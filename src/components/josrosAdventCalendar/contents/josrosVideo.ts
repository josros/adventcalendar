import { LitElement, html, customElement, TemplateResult, CSSResult, css, property, query } from 'lit-element';
import '@material/mwc-icon-button';
import './josrosContainer.ts';

@customElement('josros-video')
export class JosrosVideo extends LitElement {
  @property({ type: String })
  private src!: string;

  @query('video')
  private video!: HTMLVideoElement;

  firstUpdated(): void {
    window.addEventListener('josros-overlay-closed', () => this.video.pause());
  }

  render(): TemplateResult {
    return html`<josros-container>
      <div class="video-container">
        <slot name="top"></slot>
        <video controls>
          <source src="${this.src}" type="video/mp4" />
          Your browser does not support HTML5 video.
        </video>
        <slot name="bottom"></slot>
      </div>
    </josros-container>`;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-video--font-size, 1rem);
      }
      .video-container {
        margin: auto;
        max-width: 80em;
      }
      video {
        width: 100%;
      }
    `;
  }
}
