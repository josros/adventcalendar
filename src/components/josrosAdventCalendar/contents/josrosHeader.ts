import { LitElement, html, customElement, TemplateResult, CSSResult, css, property } from 'lit-element';
import './josrosContainer.ts';

@customElement('josros-header')
export class Header extends LitElement {
  @property({ type: String })
  private maintitle!: string;

  @property({ type: String })
  private subtitle?: string;

  render(): TemplateResult {
    return html`<josros-container>
      <h1>${this.maintitle}</h1>
      ${this.subtitle ? html`<h2>${this.subtitle}</h2>` : ''}
    </josros-container> `;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-image--font-size, 1rem);
      }
      h1 {
        font-weight: 600;
      }
      h2 {
        font-weight: 300;
      }
    `;
  }
}
