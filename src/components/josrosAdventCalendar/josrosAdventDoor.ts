import { LitElement, html, customElement, TemplateResult, CSSResult, css, property } from 'lit-element';

@customElement('josros-advent-door')
export class AdventDoor extends LitElement {
  private static readonly RESTRICT_BY_DATE = false;

  private static readonly OPENED_EVENT = 'opened';
  private static readonly CLOSED_EVENT = 'closed';

  @property({ type: Number, attribute: 'number' })
  private number!: number;

  @property({ type: Boolean })
  private open = false;

  @property({ attribute: false })
  private animateClosed = false;

  private async toggleDoor(): Promise<void> {
    if (this.open) {
      this.closeDoor();
    } else {
      this.openDoor();
    }
  }

  private async closeDoor(): Promise<void> {
    this.open = false;
    await this.timeout(550);
    const closed = new CustomEvent(AdventDoor.CLOSED_EVENT, {
      detail: this.number
    });
    this.dispatchEvent(closed);
  }

  private async openDoor(): Promise<void> {
    if (this.isOpenAllowed()) {
      this.open = true;
      await this.timeout(550);
      const opened = new CustomEvent(AdventDoor.OPENED_EVENT, {
        detail: this.number
      });
      this.dispatchEvent(opened);
    } else {
      this.animateClosed = true;
      await this.timeout(500);
      this.animateClosed = false;
    }
  }

  private timeout(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  private isOpenAllowed(): boolean {
    if (AdventDoor.RESTRICT_BY_DATE) {
      const now = new Date();
      const notBefore = new Date(Date.UTC(now.getFullYear(), 11, this.number, 0, 0, 0));
      // console.log(`notBefore: ${notBefore} now: ${now}`)
      if (notBefore.getTime() > now.getTime()) {
        return false;
      }
    }
    return true;
  }

  render(): TemplateResult {
    return html` <div class="door__bg">
      <div
        @click="${this.toggleDoor}"
        class="door ${this.open ? 'door__open' : 'door__close'} ${this.animateClosed ? 'door-closed-animation' : ''}"
      >
        <span class="door__text">${this.number}</span>
      </div>
    </div>`;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-advent-door--font-size, 1rem);
      }
      .door {
        transition: transform 0.55s linear; /* sync seconds with timeout in open/close function */
        cursor: pointer;
        text-align: center;
        line-height: 10em;
        width: 10em;
        height: 10em;
        background-image: linear-gradient(to bottom, #ef233c, #d90429);
      }
      .door__open {
        transform: rotateY(-35deg);
        transform-origin: left center;
      }
      .door__close {
        transform: rotateY(0);
        transform-origin: left center;
      }
      .door__text {
        font-size: 2.5em;
        font-weight: 500;
        color: white;
      }
      .door__bg {
        perspective: 80em;
        /* background-image: url("../static/giphy_snowing.gif"); */
        background-color: #cccccc;
      }
      .door-closed-animation {
        transform-origin: center center;
        animation-name: shake;
        animation-duration: 0.5s;
      }
      @keyframes shake {
        0% {
          transform: rotate(0);
        }
        20% {
          transform: rotate(7deg);
        }
        40% {
          transform: rotate(3deg);
        }
        60% {
          transform: rotate(10deg);
        }
        80% {
          transform: rotate(4deg);
        }
        100% {
          transform: rotate(0);
        }
      }
    `;
  }
}
