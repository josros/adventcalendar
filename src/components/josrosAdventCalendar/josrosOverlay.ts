import { LitElement, html, property, customElement, TemplateResult, CSSResult, css, query } from 'lit-element';
import { ResizeObservation } from './resizeObservation.ts';
import './josrosAdventLayout.ts';
import '@material/mwc-icon-button';

enum ContainerSize {
  DEFAULT,
  SMALL
}

@customElement('josros-overlay')
export class Overlay extends LitElement {
  private static readonly UPPER_BORDER_SMALL_PX: number = 1200;
  private static readonly CLOSED_EVENT = 'josros-overlay-closed';

  @property({ type: Boolean })
  private open = false;

  @query('.overlay')
  private container!: HTMLDivElement;

  @property({ attribute: false })
  private size?: ContainerSize;

  private onResizeLambda: () => void = () => this.onResize();
  private resizeObservation = new ResizeObservation(this.onResizeLambda);

  private close(): void {
    this.dispatchEvent(
      new CustomEvent(Overlay.CLOSED_EVENT, {
        bubbles: true,
        composed: true
      })
    );
  }

  firstUpdated(): void {
    this.resizeObservation.attach(this.container);
    this.onResize();
  }

  disconnectedCallback(): void {
    super.disconnectedCallback();
    this.resizeObservation.detach();
  }

  private onResize(): void {
    if (this.container.offsetWidth <= Overlay.UPPER_BORDER_SMALL_PX) {
      this.size = ContainerSize.SMALL;
    } else {
      this.size = ContainerSize.DEFAULT;
    }
  }

  render(): TemplateResult {
    return html`
      <div class="overlay ${this.open ? 'show' : ''}">
        <div class="overlay__content ${this.size === ContainerSize.DEFAULT ? '' : 'overlay__content--small'}">
          <slot></slot>
        </div>
        <div class="btn-container">
          <mwc-icon-button @click=${this.close} icon="cancel"></mwc-icon-button>
        </div>
      </div>
    `;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-overlay--font-size, 1rem);
      }
      .overlay {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 2;
        overflow-y: auto;
      }
      @supports (backdrop-filter: blur(10px)) {
        .overlay {
          backdrop-filter: blur(10px);
        }
      }
      .overlay.show {
        display: block;
      }
      .overlay__content {
        position: relative;
        margin: 9em;
        /* background-color: rgba(255, 255, 255, 0.95); */
        background-color: #1d1d1f;
        color: white;
        min-height: 80vh;
        overflow: hidden;
      }
      .overlay__content.overlay__content--small {
        margin: 9em 0em;
      }
      mwc-icon-button {
        color: white;
        --mdc-icon-size: 50px;
      }
      .btn-container {
        position: absolute;
        top: 1em;
        right: 3em;
      }
    `;
  }
}
