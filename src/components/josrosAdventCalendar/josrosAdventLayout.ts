import { LitElement, html, customElement, TemplateResult, CSSResult, css, query, property } from 'lit-element';
import './josrosAdventDoor.ts';
import { ResizeObservation } from './resizeObservation.ts';
import bgImg from '../../images/bg_large.jpg';
import bgImgSmall from '../../images/bg_small.jpg';

enum ContainerSize {
  DEFAULT,
  SMALL,
  XSMALL
}

@customElement('josros-advent-layout')
export class AdventLayout extends LitElement {
  private static readonly UPPER_BORDER_SMALL_PX: number = 900;
  private static readonly UPPER_BORDER_XSMALL_PX: number = 600;
  private static readonly DOOR_OPENED_EVENT = 'doorOpened';

  private static readonly STORAGE_KEY_OPEN_DOORS = 'adventcalendar:opendoors';

  private onResizeLambda: () => void = () => this.onResize();
  private resizeObservation = new ResizeObservation(this.onResizeLambda);

  @query('#layout-container')
  private container!: HTMLDivElement;

  @property({ type: Array })
  private numbers!: number[];

  @property({ attribute: false })
  private size?: ContainerSize;

  private openDoors: number[] = [];

  firstUpdated(): void {
    this.resizeObservation.attach(this.container);
    this.onResize();
    this.loadStoredDoors();
  }

  disconnectedCallback(): void {
    super.disconnectedCallback();
    this.resizeObservation.detach();
  }

  private loadStoredDoors(): void {
    const doorsFromStorage = localStorage.getItem(AdventLayout.STORAGE_KEY_OPEN_DOORS) ?? '';
    this.openDoors = JSON.parse(doorsFromStorage);
  }

  private isOpen(door: number): boolean {
    return this.openDoors.includes(door);
  }

  private onResize(): void {
    if (
      this.container.offsetWidth < AdventLayout.UPPER_BORDER_SMALL_PX &&
      this.container.offsetWidth > AdventLayout.UPPER_BORDER_XSMALL_PX
    ) {
      this.size = ContainerSize.SMALL;
    } else if (this.container.offsetWidth <= AdventLayout.UPPER_BORDER_XSMALL_PX) {
      this.size = ContainerSize.XSMALL;
    } else {
      this.size = ContainerSize.DEFAULT;
    }
  }

  private doorOpened(ev: CustomEvent): void {
    const opened = new CustomEvent(AdventLayout.DOOR_OPENED_EVENT, {
      detail: ev.detail
    });
    this.dispatchEvent(opened);
    this.addToOpenDoors(ev.detail);
  }

  private doorClosed(ev: CustomEvent): void {
    this.removeFromOpenDoors(ev.detail);
  }

  private addToOpenDoors(num: number): void {
    this.openDoors.push(num);
    localStorage.setItem(AdventLayout.STORAGE_KEY_OPEN_DOORS, JSON.stringify(this.openDoors));
  }

  private removeFromOpenDoors(num: number): void {
    const idxToRemove = this.openDoors.indexOf(num);
    this.openDoors.splice(idxToRemove, 1);
    localStorage.setItem(AdventLayout.STORAGE_KEY_OPEN_DOORS, JSON.stringify(this.openDoors));
  }

  render(): TemplateResult {
    return html`
      <style scoped>
        .background-image {
          background-image: url(${bgImgSmall});
        }
        @media only screen and (min-width: 75em) {
          .background-image {
            background-image: url(${bgImg});
          }
        }
      </style>
      <div class="main-container background-image">
        <div
          id="layout-container"
          class="grid-container ${this.size === ContainerSize.DEFAULT ? '' : this.determineSizeClass()}"
        >
          ${this.numbers.map(
            (item) =>
              html`
              <josros-advent-door .open=${this.isOpen(item)} number=${item} @opened=${this.doorOpened} @closed=${
                this.doorClosed
              }></josros-advent-door></josros-advent-door>
            `
          )}
        </div>
      </div>
    `;
  }

  private determineSizeClass(): string {
    switch (this.size) {
      case ContainerSize.SMALL:
        return 'container--small';
      case ContainerSize.XSMALL:
        return 'container--xsmall';
      default:
        return '';
    }
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-advent-layout--font-size, 1rem);
      }
      .main-container {
        min-height: 100vh;
        min-width: 25em;
        /* background-image: url(../../images/bg.jpg); */
        background-size: cover;
      }
      .grid-container {
        display: grid;

        grid-template-rows: repeat(4, min-content);
        grid-template-columns: repeat(6, minmax(min-content, 14em));
        grid-gap: 5em;
        align-items: stretch;
        justify-items: center;
        align-content: space-around;
        justify-content: center;
        padding: 2.5em;
      }

      /* width 900px */
      .grid-container.container--small {
        grid-template-rows: repeat(6, min-content);
        grid-template-columns: repeat(4, minmax(min-content, 14em));
      }

      /* width 600px */
      .grid-container.container--xsmall {
        grid-template-rows: repeat(12, min-content);
        grid-template-columns: repeat(2, minmax(min-content, 14em));
        /* background-image: linear-gradient(to bottom, red, orangered); */
      }

      /* media queries are replaced by the ResizeObserver */
      /* @media only screen and (max-width: 900px) {
        .container {
          grid-template-rows: repeat(6, min-content);
          grid-template-columns: repeat(4, minmax(min-content, 14rem));
        }
      } */
      /* @media only screen and (max-width: 600px) {
        .container {
          grid-template-rows: repeat(12, min-content);
          grid-template-columns: repeat(2, minmax(min-content, 14rem));
          background-image: linear-gradient(to bottom, red, orangered);
        }
      } */
    `;
  }
}
