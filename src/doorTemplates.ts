import { html, TemplateResult, CSSResult, css } from 'lit-element';
import './components/josrosAdventCalendar/contents/josrosContainer.ts';

export function template(door: number): TemplateResult {
  switch (door) {
    default:
      return html`<josros-container>
        <div class="not-implemented">Door not implemented</div>
      </josros-container>`;
  }
}

export function templateStyles(): CSSResult {
  return css`
    .not-implemented {
      color: red;
      font-size: 1.6em;
    }
  `;
}
